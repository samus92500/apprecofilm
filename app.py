from flask import Flask, jsonify, request
import numpy as np
import joblib

sim_matrix = joblib.load("duosim.joblib")
df = joblib.load('df.joblib')
app = Flask(__name__)


def predict(film_id, sim_matrix) :
    def find_max_matches(sim_vector, top_n = 5) :
        best_ids = np.argpartition(sim_vector, top_n)[:top_n]
        return best_ids
    return find_max_matches(sim_matrix[film_id])


@app.route("/predict", methods = ['GET'])
def get_prediction() :
    film_id = int(request.args.get('id'))

    assert film_id < df.shape[0]
    pred = predict(film_id, sim_matrix)
    response = {"original_film" : df["movie_title"].iloc[film_id]}
    [response.update({"reco_" + str(i): df['movie_title'].iloc[fid]})for i, fid in enumerate(pred)]

    return jsonify(response)


if __name__ == '__main__' :
    app.run()
